﻿using StackExchange.Redis;

public interface IRedisConnectionFactory
{
    ConnectionMultiplexer GetConnection();
}

public class RedisConnectionFactory : IRedisConnectionFactory
{
    private readonly ConnectionMultiplexer _connection;

    public RedisConnectionFactory(string connectionString)
    {
        _connection = ConnectionMultiplexer.Connect(connectionString);
    }

    public ConnectionMultiplexer GetConnection()
    {
        return _connection;
    }
}

﻿using Microsoft.Extensions.Hosting;
using System.Threading.Tasks;

namespace Tinyurl.Application
{

    public class MyCleaning : BackgroundService
    {
        private IRedisConnectionFactory _redisConnectionFactory;
        public MyCleaning(IRedisConnectionFactory redisConnectionFactory)
        {
            _redisConnectionFactory = redisConnectionFactory;
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    var connection = _redisConnectionFactory.GetConnection();
                    var db = connection.GetDatabase();
                    db.Execute("FLUSHDB");
                    await Task.Delay(TimeSpan.FromMinutes(60), stoppingToken);
                }
                catch (Exception ex)
                {
                    await Task.Delay(TimeSpan.FromSeconds(30), stoppingToken);
                }
            }
        }
    }
}

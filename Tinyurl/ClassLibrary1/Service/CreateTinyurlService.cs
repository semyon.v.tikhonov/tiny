﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tinyurl.Application.Interf;
using URL_Shortener;

namespace Tinyurl.Application.Service
{
    public  class CreateTinyurlService : ICreateUrl
    {
        private  IRedisConnectionFactory _redisConnectionFactory;
        public CreateTinyurlService(IRedisConnectionFactory redisConnectionFactory)
        {
            _redisConnectionFactory = redisConnectionFactory;
        }

        public  string Create(string longUrl)
        {
            var Uri = EncurteURL.EncurtadorUrl(new Uri(longUrl))
                .Replace("https://tinyurl.com/", "https://localhost:7100/");
            var connection = _redisConnectionFactory.GetConnection();
            var db = connection.GetDatabase();
            db.StringSet(Uri.Replace("https://localhost:7100/",""), longUrl);
                return Uri;
        }
    }
}

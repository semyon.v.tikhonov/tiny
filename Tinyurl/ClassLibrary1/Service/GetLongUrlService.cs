﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tinyurl.Application.Interface;

namespace Tinyurl.Application.Service
{
    public class GetLongUrlService : IGetLongUrl
    {
        private IRedisConnectionFactory _redisConnectionFactory;
        public GetLongUrlService(IRedisConnectionFactory redisConnectionFactory)
        {
            _redisConnectionFactory = redisConnectionFactory;
        }
        public string Get(string TinyUrl)
        {
            var connection = _redisConnectionFactory.GetConnection();
            var db = connection.GetDatabase();
            return db.StringGet(TinyUrl);
        }
    }
}

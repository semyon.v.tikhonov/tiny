﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tinyurl.Application.Interface;

namespace Tinyurl.Application.Service
{
    public class GetAllUrlService : IGetAllUrl
    {
        private IRedisConnectionFactory _redisConnectionFactory;
        public GetAllUrlService( IRedisConnectionFactory redisConnectionFactory)
        {
            _redisConnectionFactory = redisConnectionFactory;
        }

        public Dictionary<string, string> GetAllUrl()
        {
            var connection = _redisConnectionFactory.GetConnection();
            var db = connection.GetDatabase();
            var keys = connection.GetServer("localhost:6379").Keys(pattern: "*");
            var sa = keys.ToDictionary(key => key.ToString(), key => db.StringGet(key).ToString());
            return sa;
        }
    }
}

﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tinyurl.Application.Interface;

namespace Tinyurl.Application.Tinyurl
{
    public class GetAllUrl : IRequest<Dictionary<string, string>>
    {
        public GetAllUrl()
        {
        }
    }
    public class GetAllUrlHandler : IRequestHandler<GetAllUrl, Dictionary<string,string>>
    {
        private readonly IGetAllUrl _getAllUrl;
        public GetAllUrlHandler(IGetAllUrl getAllUrl)
        {
            _getAllUrl = getAllUrl;
        }

        public async Task<Dictionary<string,string>> Handle(GetAllUrl request, CancellationToken cancellationToken)
        {
            var str = _getAllUrl.GetAllUrl();
            return str;
        }
    }
}

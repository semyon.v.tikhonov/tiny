﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tinyurl.Application.Interf;
using Tinyurl.Application.Service;

namespace Tinyurl.Application.Tinyurl
{
    public class CreateTinyUrl : IRequest<string>
    {
        public string LongUrl { get; set; }
        public CreateTinyUrl(string longUrl)
        {
            LongUrl = longUrl;
        }
    }
    public class GetTinyUrlHandler : IRequestHandler<CreateTinyUrl, string>
    {
        private readonly ICreateUrl _createUrl;
        public GetTinyUrlHandler(ICreateUrl createUrl)
        {
            _createUrl = createUrl;
        }

        public async Task<string> Handle(CreateTinyUrl request, CancellationToken cancellationToken)
        {
            var str = _createUrl.Create(request.LongUrl);
            return str;
        }
    }
}
﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tinyurl.Application.Interface;

namespace Tinyurl.Application.Tinyurl
{
    public class GetLongUrl : IRequest<string>
    {
        public string TinyUrl { get; set; }
        public GetLongUrl(string tinyUrl)
        {
            TinyUrl = tinyUrl;
        }
    }
    public class GetLongUrlHandler : IRequestHandler<GetLongUrl, string>
    {
        private readonly IGetLongUrl _getLongUrl;
        public GetLongUrlHandler(IGetLongUrl getLongUrl)
        {
            _getLongUrl = getLongUrl;
        }

        public async Task<string> Handle(GetLongUrl request, CancellationToken cancellationToken)
        {
            var tra = _getLongUrl.Get(request.TinyUrl);
            return _getLongUrl.Get(request.TinyUrl);
            
        }
    }
}

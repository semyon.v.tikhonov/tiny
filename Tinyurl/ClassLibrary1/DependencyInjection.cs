﻿using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Tinyurl.Application.Interf;
using Tinyurl.Application.Interface;
using Tinyurl.Application.Service;

namespace Tinyurl.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(Assembly.GetExecutingAssembly()));
            services.AddTransient<ICreateUrl, CreateTinyurlService>();
            services.AddTransient<IGetLongUrl, GetLongUrlService>();
            services.AddTransient<IGetAllUrl, GetAllUrlService>();
            services.AddHostedService<MyCleaning>();
            return services;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tinyurl.Application.Interf
{
    public interface ICreateUrl
    {
        string Create(string LongUrl);
    }
}

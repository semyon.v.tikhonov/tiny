﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tinyurl.Application.Interface
{
     public interface IGetAllUrl
    {
        Dictionary<string, string> GetAllUrl();
    }
}

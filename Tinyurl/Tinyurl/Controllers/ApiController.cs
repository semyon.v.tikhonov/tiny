﻿using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Tinyurl.Controllers
{
    public abstract class ApiController : ControllerBase
    {
        private IMediator _mediator;

        public IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();
    }
}

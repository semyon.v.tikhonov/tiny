﻿using MediatR;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Tinyurl.Application.Tinyurl;

namespace Tinyurl.Controllers
{
    [ApiController]
    public class RedirectController : ApiController
    {
        [HttpGet("{shortUrl}")]
        public  async Task<IActionResult> Index(string shortUrl)
        {
            return Redirect( await  Mediator.Send(new GetLongUrl(Uri.UnescapeDataString(shortUrl))));
        }
    }
}

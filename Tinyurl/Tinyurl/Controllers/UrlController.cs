using MediatR;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Tinyurl.Application.Tinyurl;

namespace Tinyurl.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UrlController : ApiController
    {
        private readonly ILogger<UrlController> _logger;

        public UrlController(ILogger<UrlController> logger)
        {
            _logger = logger;
        }

        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpPost]
        public async Task<string> Create([FromBody] string longUrl) =>
            await Mediator.Send(new CreateTinyUrl(longUrl));

        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet]
        public async Task<Dictionary<string,string>> GetAllUrl() =>
            await Mediator.Send(new GetAllUrl());
    }
}
using FluentScheduler;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Configuration;
using System.Reflection;
using Tinyurl.Application;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()));
builder.Services.AddApplication();
builder.Services.AddStackExchangeRedisCache(options => {
    options.Configuration = builder.Configuration.GetConnectionString("Redis");
    options.InstanceName = "RedisDemo_";
});
builder.Services.AddSingleton<IRedisConnectionFactory>(new RedisConnectionFactory(builder.Configuration.GetConnectionString("Redis")));
builder.Services.AddHostedService<MyCleaning>();
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: "AllowSpecificOrigins",
        policy =>
        {
            policy.AllowAnyOrigin()
            .AllowAnyHeader()
            .AllowAnyMethod();
        });
});
var app = builder.Build();
app.UseCors("AllowSpecificOrigins");
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseRouting();
app.UseEndpoints(endpoints =>
{
    endpoints.MapControllerRoute(
        name: "RedirectShortUrl",
        pattern: "s/{shortUrl}",
        defaults: new { controller = "Redirect", action = "Index" });
});
app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();

app.Run();

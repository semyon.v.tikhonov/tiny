﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tinyurl.Domain.Entities
{
    public class Url
    {
        public int id { get; set; }
        public Dictionary<string,string> LongUrlwithTinyUrl { get; set; }
        
    }
}

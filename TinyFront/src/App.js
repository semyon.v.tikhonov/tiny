import './App.css';
import MyComponent from './MyTiny/MyComponent'; 
import BasicExample from './MyTiny/TableUrl'; 
function App() {
  return (
    <div className="App">
      <MyComponent/>
      <BasicExample/>
    </div>
  );
}

export default App;

import axios from 'axios';
import React, { useEffect, useState } from "react";
import 'semantic-ui-css/semantic.min.css';
import './MyComponent.css';

function MyComponent() {
  const [url, setUrl] = useState('');
  const [responseText, setResponseText] = useState('');

  const InputExampleInput = (event) => {
    setUrl(event.target.value)
  }

  const handleSubmit = () => {
    axios.post('https://localhost:7100/Url', `"${url}"`, {
      headers: {
        'Accept': 'text/plain',
        'Content-Type': 'application/json'
      }
    })
    .then(response => {
      setResponseText(response.data);
      console.log(response.data);
    })
    .catch(error => {
      console.error('Ошибка при выполнении запроса:', error);
    });
  };

  return (
    <div className="my-component">
      <div className="ui input">
        <input type="text" placeholder="LongUrl..." value={url} onChange={InputExampleInput}/>
      </div>
      <div className="ui input">
        <input type="text" placeholder="Tiny" value={responseText} />
      </div>
      <button onClick={handleSubmit}>Shorten URL</button>
    </div>
  );
}

export default MyComponent;
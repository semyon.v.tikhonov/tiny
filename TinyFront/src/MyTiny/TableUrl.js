import Accordion from 'react-bootstrap/Accordion';
import './BasicExample.css'; // Импортируем файл стилей
import React, { useState, useEffect } from 'react';
import axios from 'axios';
function BasicExample() {
    const [data, setData] = useState([]);

  useEffect(() => {
    // Выполняем GET-запрос на сервер при монтировании компонента
    axios.get('https://localhost:7100/Url')
      .then(response => {
        setData(response.data); // Сохраняем полученные данные в состояние
      })
      .catch(error => {
        console.error('Ошибка при выполнении GET-запроса:', error);
      });
  }, []);
  const addPrefixToHeader = (key) => {
    return "https://localhost:7100/" + key;
  }
  return (
    <div className="sidebar">
      <Accordion defaultActiveKey="0">
        {Object.entries(data).map(([key, value], index) => (
          <Accordion.Item key={index} eventKey={index.toString()}>
            <Accordion.Header className="accordion-header"><a href={addPrefixToHeader(key)} target="_blank" rel="noopener noreferrer">{key}</a></Accordion.Header>
            <Accordion.Body>
            <a href={value} target="_blank" rel="noopener noreferrer">{value}</a>
            </Accordion.Body>
          </Accordion.Item>
        ))}
      </Accordion>
    </div>
  );
}

export default BasicExample;
## Функционал

- Генерация короткой ссылки из длинной.
- Отображение всех имеющихся ссылок в базе данных.

## Использованные технологии

- Фронтенд: React.js (самый простенький интерфейс).
- База данных: Redis (развернут в Docker контейнере).
- Паттерн: Mediator (для обработки запросов).
- Авторизация: Авторизация не реализована из-за ограниченного времени.(увидел тестовое за два дня до сдачи)

## Автоочистка базы данных

- База данных Redis автоматически очищается раз в час.

## Использование

1. После запуска приложения, перейдите на главную страницу.
2. Введите длинную ссылку в форму и нажмите кнопку "Сократить" для получения короткой ссылки.
3. Все имеющиеся короткие ссылки будут отображены на странице.

## Контакты

Если у вас есть вопросы или комментарии, свяжитесь со мной в телеграме https://t.me/semenAlesa
